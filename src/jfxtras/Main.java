package jfxtras;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import jfxtras.controller.CalendarController;
import jfxtras.internal.scene.control.skin.agenda.base24hour.AppointmentIO;
import jfxtras.scene.control.agenda.Agenda.AppointmentGroup;
import jfxtras.scene.control.agenda.AppointmentFactory;
import jfxtras.scene.control.agenda.Repeat;
import jfxtras.scene.control.agenda.Settings;

public class Main extends Application {
	
    public MyData data = new MyData();  // All data - appointments, styles, members, etc.
    
    public static void main(String[] args) {
        launch(args);       
    }

	@Override
	public void start(Stage primaryStage) throws IOException, TransformerException, ParserConfigurationException, SAXException {
	    
        Locale myLocale = Locale.getDefault();
        ResourceBundle resources = ResourceBundle.getBundle("jfxtras.Bundle", myLocale);
        Settings.setup(resources);

        ObservableList<AppointmentGroup> appointmentGroups = AppointmentIO.readAppointmentGroups(Settings.APPOINTMENT_GROUPS_FILE.toFile());
        data.setAppointmentGroups(appointmentGroups);
        Repeat.readFromFile(Settings.APPOINTMENT_REPEATS_FILE, appointmentGroups, data.getRepeats());
        AppointmentFactory.setupRepeatMap(data.getRepeats()); // must be done before appointments are read
        AppointmentFactory.readFromFile(appointmentGroups, data.getAppointments());
        data.getRepeats().stream().forEach(a -> a.collectAppointments(data.getAppointments())); // add individual appointments that have repeat rules to their Repeat objects
        data.getRepeats().stream().forEach(a -> a.makeAppointments(data.getAppointments())); // Make repeat appointments
	    
        // ROOT PANE
        FXMLLoader mainLoader = new FXMLLoader();
        mainLoader.setLocation(Main.class.getResource("view/Calendar.fxml"));
        BorderPane root = mainLoader.load();
        CalendarController controller = mainLoader.getController();
        controller.setupData(data);

        Scene scene = new Scene(root, 1366, 768);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Repeatable Agenda Demo");
        primaryStage.show();
    }
	
}

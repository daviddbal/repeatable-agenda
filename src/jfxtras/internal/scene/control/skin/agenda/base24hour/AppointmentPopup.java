package jfxtras.internal.scene.control.skin.agenda.base24hour;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Popup;
import jfxtras.Main;
import jfxtras.internal.scene.control.skin.agenda.base24hour.controller.AppointmentPopupController;
import jfxtras.scene.control.agenda.Agenda.Appointment;
import jfxtras.scene.control.agenda.Settings;
import jfxtras.util.NodeUtil;

public class AppointmentPopup extends Popup {

    private AnchorPane appointmentManage;
    
    public AppointmentPopup(Pane pane
            , Appointment appointment
            , LayoutHelp layoutHelp) {
        
        // LOAD FXML
        FXMLLoader appointmentManageLoader = new FXMLLoader();
        appointmentManageLoader.setLocation(Main.class.getResource("internal/scene/control/skin/agenda/base24hour/view/AppointmentPopup.fxml"));
        appointmentManageLoader.setResources(Settings.resources);
        try {
            appointmentManage = appointmentManageLoader.load();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
                
        appointmentManage.getStylesheets().add(layoutHelp.skinnable.getUserAgentStylesheet());
        
        AppointmentPopupController appointmentManageController = appointmentManageLoader.getController();
        appointmentManageController.setupData(pane, appointment, layoutHelp, this);
//        AppointmentManageController.setupData(appointment, layoutHelp, newStage);
//        Scene scene2 = new Scene(appointmentMenuPane);
        
        System.out.println("sizes " + NodeUtil.screenY(pane) + " " + pane.getHeight());

        
        this.setAutoFix(true);
        this.setAutoHide(true);
        this.setHideOnEscape(true);
        this.getContent().add(appointmentManage);
        
        this.setX(NodeUtil.screenX(pane));
        this.setY(NodeUtil.screenY(pane) - pane.getHeight());
//        layoutHelp.skinnable.getUserAgentStylesheet();
        
//        appointmentManage.getStyleClass().add("Agenda" + "Popup");
        
        this.setOnHidden( (windowEvent) -> {
            System.out.println("close menage popup");
        });
        
        
    }
}
